
var hostname, port;

if (process.argv.length == 3) {
	hostname = process.argv[2];
	port = 80;
} else if (process.argv.length == 4) {
	hostname = process.argv[2];
	port = process.argv[3];
} else {
	var path = require('path');
	console.log('Usage: ', process.argv[0], path.basename(process.argv[1]), 'hostname <port>');
	process.exit();
}

var io = require('socket.io-client');
var socket = io.connect(hostname, {port: port});

socket.on('changeStatus', function(data) {
	if (data == 'On') {
		// Turn the lights on
	} else if (data == 'Off') {
		// Turn the lights off
	}
	console.log('status: ' + data.status);
});

socket.on('connect', function(data) {
	console.log('Connected');
});

socket.on('connecting', function(data) {
	console.log('Connecting...');
});
